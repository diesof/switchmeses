﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchMeses
{
    class SwitchMeses
    {
        static void Main(string[] args)
        {
            int numeroMes;

            Console.Write("Ingrese el número de mes en el cual se encuentra: ");
            numeroMes = int.Parse(Console.ReadLine());

            switch (numeroMes)
            {
                case 1: Console.Write("Se encuentra en el mes de Enero :D"); break;
                case 2: Console.Write("Se encuentra en el mes de Febrero :D"); break;
                case 3: Console.Write("Se encuentra en el mes de Marzo :D"); break;
                case 4: Console.Write("Se encuentra en el mes de Abril :D"); break;
                case 5: Console.Write("Se encuentra en el mes de Mayo :D"); break;
                case 6: Console.Write("Se encuentra en el mes de Junio :D"); break;
                case 7: Console.Write("Se encuentra en el mes de Julio :D"); break;
                case 8: Console.Write("Se encuentra en el mes de Agoso :D"); break;
                case 9: Console.Write("Se encuentra en el mes de Septiembre :D"); break;
                case 10: Console.Write("Se encuentra en el mes de Octubre :D"); break;
                case 11: Console.Write("Se encuentra en el mes de Noviembre :D"); break;
                case 12: Console.Write("Se encuentra en el mes de Diciembre :D"); break;
                default: Console.Write("El número ingresado no pertenec a ningún mes"); break;
            }

            Console.ReadKey();
        }
    }
}